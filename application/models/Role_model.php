<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Role_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    private function check_assign($action_id, $user_id)
    {
      $query = $this->db->where('action_id', $action_id)
                        ->where('user_id', $user_id)
                        ->limit(1)
                        ->get('acl');

      if ($query->num_rows() == 1) {
          return TRUE;
      }
      else {
          return FALSE;
      }
    }

    public function insert_assign()
    {
      $count = count($this->input->post('user_id'));
      for ($i = 0; $i < $count; $i++) {
          $action_id = $this->input->post('action_id');
          $user_id = $this->input->post('user_id')[$i];

          $data = array(
            'action_id' => $action_id,
            'user_id' => $user_id
          );
          // check if this user exist in this action_id
          $check = $this->check_assign($action_id, $user_id);
          if($check == FALSE) $this->db->insert('acl', $data);
       }

       if($this->db->affected_rows() === 1)
       {
           return TRUE;
       }
       else
       {
           return FALSE;
       }

    }

    public function insert_category()
    {
      $data = array(
          'category_code' => strtolower($this->input->post('category_code')),
          'category_desc' => ucfirst(strtolower($this->input->post('category_desc')))
      );
      $this->db->insert('acl_categories', $data);

      if($this->db->affected_rows() === 1)
      {
          $category_id = $this->db->insert_id();
          $action_code = '*';
          $action_desc = 'All Actions';
          $this->insert_action($category_id, $action_code, $action_desc);
          return TRUE;
      }
      else
      {
          return FALSE;
      }
    }

    public function insert_action($category_id, $action_code, $action_desc)
    {
      $data = array(
          'action_code' => strtolower($action_code),
          'action_desc' => ucfirst(strtolower($action_desc)),
          'category_id' => $category_id
      );
      $this->db->insert('acl_actions', $data);

      if($this->db->affected_rows() === 1)
      {
          return TRUE;
      }
      else
      {
          return FALSE;
      }
    }

    public function category()
    {
      return $this->db->get('acl_categories');
    }


    public function checkCategory($category_id)
    {
        $query = $this->db->where('category_id', $category_id)
                          ->limit(1)
                          ->get('acl_categories');

        if ($query->num_rows() == 1) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function action()
    {
      $this->db->select('*');
      $this->db->from('acl_actions');
      $this->db->join('acl_categories', 'acl_categories.category_id = acl_actions.category_id');
      return $this->db->get();
    }
}
