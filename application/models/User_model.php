<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
  public function __construct()
  {
      parent::__construct();
  }

  public function getUser()
  {
    $this->db->select('user_id, username, email, last_login, created_at');
    $this->db->order_by('username', 'ASC');
    $this->db->from('users');
    return $this->db->get();
  }

  public function checkUser($user_id)
  {
      $query = $this->db->where('user_id', $user_id)
                        ->limit(1)
                        ->get('users');

      if ($query->num_rows() == 1) {
          return TRUE;
      }
      else {
          return FALSE;
      }
  }

}
