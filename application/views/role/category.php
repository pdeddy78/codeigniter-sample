<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $flash_message = $this->session->flashdata('flash_message');?>
<?php if(!empty($flash_message)) { ?>
  <div class="ui compact message">
    <i class="close icon"></i>
    <p><?=$flash_message?></p>
  </div>
<?php } ?>
<h1 class="text-light"><?=$title?><span class="mif-cogs place-right"></span></h1>
<hr class="thin bg-grayLighter">
<div class="example" data-text="Category">
  <?=validation_errors('<div class="fg-crimson">', '</div>'); ?>
  <?=form_open('', array('id' => 'Fm_category', 'class'=>''))?>
  <div class="grid">
    <div class="row cells3">
      <div class="cell">
        <?=form_label('Category', 'category_code');?>
        <div class="input-control text">
            <?=form_input(array('name' => 'category_code', 'placeholder' => 'Category', 'value' => strtolower(set_value('category_code')), 'required'=>'required'))?>
        </div>
      </div>
      <div class="cell">
        <?=form_label('General Permissions', 'category_desc');?>
        <div class="input-control text">
            <?=form_input(array('name' => 'category_desc', 'placeholder' => 'General Permissions', 'value' => ucfirst(strtolower(set_value('category_desc'))), 'required'=>'required'))?>
        </div>
      </div>
      <div class="cell">
        <label>&nbsp</label>
        <div class="input-control text">
            <?=form_submit('submit','Add', array('class'=>'button primary'))?>
        </div>
      </div>
    </div>
  </div>
  <?=form_close()?>
</div>
<table class="dataTable border bordered" data-role="datatable" data-auto-width="false">
    <thead>
      <tr>
          <th width="5%">No</th>
          <th>Category</th>
          <th>Description</th>
          <th width="5%">Act</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=''; foreach ($category->result() as $row) { $no++; ?>
      <tr>
        <td><?=$no?></td>
        <td><?=$row->category_code?></td>
        <td><?=$row->category_desc?></td>
        <td>
          <a class="button" title="Assign" href="<?=base_url().'role/category/assign/'.$row->category_id?>"><span class="mif-command"></span></a>
        </td>
      </tr>
      <?php } ?>
    </tbody>
</table>
