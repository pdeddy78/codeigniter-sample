<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h1 class="text-light"><?=$title?><span class="mif-cogs place-right"></span></h1>
<hr class="thin bg-grayLighter">
<div class="example" data-text="Assign">
  <?=validation_errors('<div class="fg-crimson">', '</div>'); ?>
  <?=form_open('role/category/assign_add', array('id' => 'Fm_category_assign', 'class'=>''))?>
  <?=form_hidden('action_id',$action_id)?>
  <div class="grid">
    <div class="row">
    <?php foreach($user->result() as $row) { ?>
      <label class="input-control checkbox">
      <?php $data = array(
            'name'      => 'user_id[]',
            'value'     => $row->user_id,
            'checked'   => FALSE,
          );
            echo form_checkbox($data);
        ?>
        <span class="check"></span>
        <span class="caption"><?=$row->username?></span>
      </label>
    <?php } ?>
    </div>
    <div class="row cells2">
      <div class="input-control">
          <?=form_submit('submit','Assign', array('class'=>'button primary'))?>
      </div>
      &nbsp;
      <div class="input-control">
        <a class="button info" href="<?=base_url()?>role/category"><span class="icon mif-arrow-left"></span> Back</a>
      </div>
    </div>
  </div>
  <?=form_close()?>
</div>
