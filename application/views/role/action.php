<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h1 class="text-light"><?=$title?><span class="mif-cogs place-right"></span></h1>
<hr class="thin bg-grayLighter">
<div class="example" data-text="Action">
  <?=validation_errors('<div class="fg-crimson">', '</div>'); ?>
  <?=form_open('', array('id' => 'Fm_action', 'class'=>''))?>
  <div class="grid">
    <div class="row cells4">
      <div class="cell">
        <?=form_label('Action', 'action_code');?>
        <div class="input-control text">
            <?=form_input(array('name' => 'action_code', 'placeholder' => 'Action', 'value' => strtolower(set_value('action_code'))))?>
        </div>
      </div>
      <div class="cell">
        <?=form_label('Description', 'action_desc');?>
        <div class="input-control text">
            <?=form_input(array('name' => 'action_desc', 'placeholder' => 'Description', 'value' => ucfirst(strtolower(set_value('action_desc')))))?>
        </div>
      </div>
      <div class="cell">
        <?=form_label('Category', 'category_desc');?>
        <div class="input-control text">
            <select class="form-control" name="category_id">
              <option value="">-</option>
                  <?php foreach($category->result() as $row) { ?>
                      <option value="<?=$row->category_id?>" <?=set_select('category_id', $row->category_id)?>><?=$row->category_code?></option>
                  <?php } ?>
            </select>
        </div>
      </div>
      <div class="cell">
        <label>&nbsp</label>
        <div class="input-control text">
            <?=form_submit('submit','Add', array('class'=>'button primary'))?>
        </div>
      </div>
    </div>
  </div>
  <?=form_close()?>
</div>
<table class="dataTable border bordered" data-role="datatable" data-auto-width="false">
    <thead>
      <tr>
          <th width="5%">No</th>
          <th>Action</th>
          <th>Description</th>
          <th>Category</th>
          <th>Category Description</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=''; foreach ($action->result() as $row) { $no++; ?>
      <tr>
        <td><?=$no?></td>
        <td><?=$row->action_code?></td>
        <td><?=$row->action_desc?></td>
        <td><?=$row->category_code?></td>
        <td><?=$row->category_desc?></td>
      </tr>
      <?php } ?>
    </tbody>
</table>
