<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h1 class="text-light"><?=$title?><span class="mif-cogs place-right"></span></h1>
<hr class="thin bg-grayLighter">
<div class="example" data-text="Sp. Permits">
  <?=validation_errors('<div class="fg-crimson">', '</div>'); ?>
  <?=form_open('role/special/assign_add', array('id' => 'Fm_category_assign', 'class'=>''))?>
  <?=form_hidden('user_id',$user_id)?>
  <div class="grid">
    <div class="row">
      <?php foreach($action->result() as $row) { ?>
        <label class="input-control checkbox">
        <?php $data = array(
              'name'      => 'action_id[]',
              'value'     => $row->action_id,
              'checked'   => FALSE,
            );
              echo form_checkbox($data);
          ?>
          <span class="check"></span>
          <span class="caption"><?=$row->category_code?>.<?=$row->action_code?></span>
        </label>
      <?php } ?>
    </div>
    <div class="row cells2">
      <div class="input-control">
          <?=form_submit('submit','Assign', array('class'=>'button primary'))?>
      </div>
      &nbsp;
      <div class="input-control">
        <a class="button info" href="<?=base_url()?>role/special"><span class="icon mif-arrow-left"></span> Back</a>
      </div>
    </div>
  </div>
  <?=form_close()?>
</div>
