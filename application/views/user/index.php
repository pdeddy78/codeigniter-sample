<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h1 class="text-light"><?=$title?><span class="mif-users place-right"></span></h1>
<hr class="thin bg-grayLighter">
<table class="dataTable border bordered" data-role="datatable" data-auto-width="false">
    <thead>
      <tr>
          <th width="5%">No</th>
          <th>Username</th>
          <th>Email</th>
          <th>Last Login</th>
          <th>Created At</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=''; foreach ($user->result() as $row) { $no++; ?>
      <tr>
        <td><?=$no?></td>
        <td><?=$row->username?></td>
        <td><?=$row->email?></td>
        <td><?=$row->last_login?></td>
        <td><?=$row->created_at?></td>
      </tr>
      <?php } ?>
    </tbody>
</table>
