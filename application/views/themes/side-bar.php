  <ul class="sidebar darcula">
      <li class="<?php if(isset($active_home)) echo $active_home;?>"><a href="<?=base_url()?>">
          <span class="mif-apps icon"></span>
          <span class="title">Home</span>
      </a></li>
      <li class="<?php if(isset($active_user)) echo $active_user;?>"><a href="<?=base_url()?>user">
          <span class="mif-users icon"></span>
          <span class="title">Users</span>
      </a></li>
      <?php if($auth_level >= 9) {?>
      <li class="<?php if(isset($active_role)) echo $active_role?>">
          <a class="dropdown-toggle" href="#" title="Role Options">
            <span class="mif-cogs icon"></span>
            <span class="title">Manage Role</span>
          </a>
          <ul class="sidebar subdown" data-role="dropdown">
              <li class="<?php if(isset($active_role_cat)) echo $active_role_cat?>"><a href="<?=base_url()?>role/category"><span class="mif-power icon"></span><span class="title">Category</span></a></li>
              <li class="<?php if(isset($active_role_act)) echo $active_role_act?>"><a href="<?=base_url()?>role/action"><span class="mif-key icon"></span><span class="title">Action</span></a></li>
              <li class="<?php if(isset($active_role_sp)) echo $active_role_sp?>"><a href="<?=base_url()?>role/special"><span class="mif-user-check icon"></span><span class="title">Special Permits</span></a></li>
          </ul>
      </li>
      <?php } ?>
  </ul>
