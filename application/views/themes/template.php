<!DOCTYPE html>
<html>
  <head>
  <?php $this->load->view('themes/header');?>
  </head>
  <body class="bg-navy">
    <div class="page-content">
      <?php $this->load->view('themes/app-bar');?>
      <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
          <div class="cell size-x200" id="cell-sidebar" style="background-color: #262626; height: 100%">
            <?php $this->load->view('themes/side-bar');?>
          </div>
          <div class="cell auto-size padding20 bg-navy" id="cell-content">
            <?php $this->load->view($main_view);?>
          </div>
        </div>
      </div>
    </div>
  </body>
  <?php $this->load->view('themes/footer');?>
</html>
