<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="MIS Team">
<meta name="keywords" content="">
<meta name="author" content="pdeddy78">

<link rel='shortcut icon' type='image/x-icon' href='<?=base_url()?>favicon.ico' />

<title><?=$title?> | <?=$this->config->item('app_name')?></title>
<?php
  $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
  $this->session->set_userdata('hostname', $hostname);
?>
<link href="<?=base_url()?>assets/css/metro.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/metro-schemes.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/metro-icons.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/metro-responsive.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/message.min.css" rel="stylesheet">
<?php
  if( isset( $javascripts ) )
  {
    foreach( $javascripts as $js )
    {
      echo '<script src="' . $js . '"></script>' . "\n";
    }
  }
?>

<style>
    html, body {
        height: 100%;
    }
    body {
    }
    .page-content {
        padding-top: 3.125rem;
        min-height: 100%;
        height: 100%;
    }
    .table .input-control.checkbox {
        line-height: 1;
        min-height: 0;
        height: auto;
    }
    @media screen and (max-width: 800px){
        #cell-sidebar {
            flex-basis: 52px;
        }
        #cell-content {
            flex-basis: calc(100% - 52px);
        }
    }
    #preloader{
        width:100%;
        height:100%;
        position:fixed;
        z-index:9999
    }
</style>

<script>
    $('.message .close')
    .on('click', function() {
      $(this)
        .closest('.message')
        .transition('fade')
      ;
    })

    function pushMessage(t){
        var mes = 'Info|tes';
        $.Notify({
            caption: mes.split("|")[0],
            content: mes.split("|")[1],
            type: t
        });
    }

    $(function(){
        $('.sidebar').on('click', 'li', function(){
            if (!$(this).hasClass('active')) {
                $('.sidebar li').removeClass('active');
                $(this).addClass('active');
            }
        })
    })
</script>
