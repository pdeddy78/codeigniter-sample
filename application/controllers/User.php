<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model','',TRUE);
	}

	public function index()
	{
		if( $this->verify_min_level(9) )
		{
			$user = $this->User_model->getUser();
			$data = array (
					'title' 					=> 'Users',
					'active_user'			=> 'active',
					'main_view' 			=> 'user/index',
					'user' 						=> $user
				);
				$data['javascripts'] = [
					base_url().'assets/js/jquery-2.1.3.min.js',
					base_url().'assets/js/jquery.dataTables.min.js',
					base_url().'assets/js/metro.min.js'
				];
				$this->load->view('themes/template', $data);
		}
		else redirect('home');
	}



}
