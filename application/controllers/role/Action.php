<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Role_model','',TRUE);
	}

	public function index()
	{
		if( $this->verify_min_level(9) )
		{
			$this->form_validation->set_rules('action_code', 'Action', 'trim|required|alpha_dash',
        array(
            'required'      => 'You have not provided %s.'
        )
			);
			$this->form_validation->set_rules('action_desc', 'Action Description', 'trim|required|alpha_numeric_spaces');
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');

			if ($this->form_validation->run() == FALSE)
      {
				$category = $this->Role_model->category();
				$action = $this->Role_model->action();

				$data = array (
					'title' 					=> 'Manage Role',
					'active_role'			=> 'active',
					'active_role_act' => 'active',
					'main_view' 			=> 'role/action',
					'category' 				=> $category,
					'action'					=> $action
				);
				$data['javascripts'] = [
					base_url().'assets/js/jquery-2.1.3.min.js',
					base_url().'assets/js/jquery.dataTables.min.js',
					base_url().'assets/js/metro.min.js'
				];
				$this->load->view('themes/template', $data);
			}
			else
			{
				$category_id = $this->input->post('category_id');
				$action_code = $this->input->post('action_code');
				$action_desc = $this->input->post('action_desc');

				$insert = $this->Role_model->insert_action($category_id, $action_code, $action_desc);
				if($insert)
      	{
            $this->session->set_flashdata('message', 'success.');
      			redirect($this->uri->uri_string());
      	}
        else die('Failed to insert data.');
			}
		}
		else redirect('home');
	}

}
