<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Role_model','',TRUE);
		$this->load->model('User_model','',TRUE);
	}

	public function index()
	{
		if( $this->verify_min_level(9))	{
			$this->form_validation->set_rules('category_code', 'Category', 'trim|required|is_unique[acl_categories.category_code]|alpha_dash',
        array(
            'required'      => 'You have not provided %s.',
            'is_unique'     => 'This %s already exists.'
        )
			);
			$this->form_validation->set_rules('category_desc', 'General Permissions', 'trim|required|alpha_numeric_spaces');

			if ($this->form_validation->run() == FALSE) {
				$category = $this->Role_model->category();
				$action = $this->Role_model->action();

				$data = array (
					'title' 					=> 'Manage Role',
					'active_role'			=> 'active',
					'active_role_cat' => 'active',
					'main_view' 			=> 'role/category',
					'category' 				=> $category,
					'action'					=> $action
				);
				$data['javascripts'] = [
					base_url().'assets/js/jquery-2.1.3.min.js',
					base_url().'assets/js/jquery.dataTables.min.js',
					base_url().'assets/js/metro.min.js'
				];

				$this->load->view('themes/template', $data);
			}

			else {
				$insert = $this->Role_model->insert_category();
				if($insert) {
            $this->session->set_flashdata('message', 'success.');
      			redirect($this->uri->uri_string());
      	}
        else die('Failed to insert data.');
			}
		}
		else redirect('home');
	}

	public function assign($category_id = NULL)
	{
		if( $this->verify_min_level(9) )
		{
			$category_id = $this->uri->segment(4);
			if($category_id == NULL) {
					die('category kosong');
			}
			else
			{
				$category = $this->Role_model->checkCategory($category_id);
				if($category)	{
					$user = $this->User_model->getUser();
					$data = array (
						'title' 					=> 'Assign Role',
						'active_role'			=> 'active',
						'active_role_cat' => 'active',
						'main_view' 			=> 'role/assign',
						'user'						=> $user,
						'action_id'				=> $category_id
					);
					$data['javascripts'] = [
						base_url().'assets/js/jquery-2.1.3.min.js',
						base_url().'assets/js/jquery.dataTables.min.js',
						base_url().'assets/js/metro.min.js'
					];
					$this->load->view('themes/template', $data);
				}
				else {
					$this->session->set_flashdata('flash_message', 'Sorry gagal');
					redirect('role/category');
				}

			}
		}
		else redirect('home');
	}

	public function assign_add()
	{
		if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' )
				$this->require_min_level(1);

		$this->form_validation->set_rules('action_id', 'Action', 'trim|required|numeric');
		$this->form_validation->set_rules('user_id[]', 'Action', 'trim|numeric');

		if ($this->form_validation->run() == FALSE)
		{
			die('gagal');
		}
		else
		{
			//$sql1="INSERT INTO $tbl_name2 (name, lastname, email)VALUES('{$_POST['name'][$i]}', '{$_POST['lastname'][$i]}', '{$_POST['email'][$i]}')";
			//$user_id = implode(", ",$this->input->post('user_id[]'));
			$insert = $this->Role_model->insert_assign();
			if($insert) {
					$this->session->set_flashdata('flash_message', 'Success');
					redirect('role/category');
			}
			else die('Failed to insert data.');

		}
	}
}
