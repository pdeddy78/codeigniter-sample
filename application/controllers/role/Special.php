<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Special extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Role_model','',TRUE);
		$this->load->model('User_model','',TRUE);
	}

	public function index()
	{
		if( $this->verify_min_level(9) )
		{
			$this->form_validation->set_rules('action_code', 'Action', 'trim|required|alpha_dash',
        array(
            'required'      => 'You have not provided %s.'
        )
			);
			$this->form_validation->set_rules('action_desc', 'Action Description', 'trim|required|alpha_numeric_spaces');
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');

			if ($this->form_validation->run() == FALSE)
      {
				$category = $this->Role_model->category();
				$action = $this->Role_model->action();
				$user = $this->User_model->getUser();

				$data = array (
					'title' 					=> 'Manage Role',
					'active_role'			=> 'active',
					'active_role_sp'  => 'active',
					'main_view' 			=> 'role/special',
					'category' 				=> $category,
					'action'					=> $action,
					'user'						=> $user
				);
				$data['javascripts'] = [
					base_url().'assets/js/jquery-2.1.3.min.js',
					base_url().'assets/js/jquery.dataTables.min.js',
					base_url().'assets/js/metro.min.js'
				];
				$this->load->view('themes/template', $data);
			}
			else
			{
				$category_id = $this->input->post('category_id');
				$action_code = $this->input->post('action_code');
				$action_desc = $this->input->post('action_desc');

				$insert = $this->Role_model->insert_action($category_id, $action_code, $action_desc);
				if($insert)
      	{
            $this->session->set_flashdata('message', 'success.');
      			redirect($this->uri->uri_string());
      	}
        else die('Failed to insert data.');
			}
		}
		else redirect('home');
	}

	public function assign($user_id = NULL)
	{
		if( $this->verify_min_level(9) )
		{
			$user_id = $this->uri->segment(4);
			if($user_id == NULL) {
					die('user kosong');
			}
			else
			{
				$user = $this->User_model->checkUser($user_id);
				if($user)	{
					$action = $this->Role_model->action();
					$data = array (
						'title' 					=> 'Assign Role',
						'active_role'			=> 'active',
						'active_role_cat' => 'active',
						'main_view' 			=> 'role/special_permits',
						'user_id'					=> $user_id,
						'action'					=> $action
					);
					$data['javascripts'] = [
						base_url().'assets/js/jquery-2.1.3.min.js',
						base_url().'assets/js/jquery.dataTables.min.js',
						base_url().'assets/js/metro.min.js'
					];
					$this->load->view('themes/template', $data);
				}
				else {
					$this->session->set_flashdata('flash_message', 'Sorry gagal');
					redirect('role/special');
				}

			}
		}
		else redirect('home');
	}

	public function assign_add()
	{
		if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' )
				$this->require_min_level(1);

		$this->form_validation->set_rules('user_id', 'Action', 'trim|required|numeric');
		$this->form_validation->set_rules('action_id[]', 'Action', 'trim|numeric');

		if ($this->form_validation->run() == FALSE)
		{
			die('gagal');
		}
		else
		{
			var_dump($_POST);

		}
	}

}
