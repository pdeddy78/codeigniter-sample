<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		/* for testing purpose */
		/* $this->load->model('Report_model', '', TRUE); */
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function login()
	{
		if( $this->verify_min_level(6) )
		{
			// The logged in user's user ID
			echo $this->auth_user_id;
			// The logged in user's username
			echo $this->auth_username;
			// The logged in user's account level by number
			echo $this->auth_level;
			// The logged in user's account level by name
			echo $this->auth_role;
			// The logged in user's email address
			echo $this->auth_email;
			// Any ACL permissions applied to the user
			echo $this->acl;
		}

		redirect('home/login');
	}

}
